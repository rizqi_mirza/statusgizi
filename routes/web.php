<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('pages.homepage');
});

//untuk regristrasi siswa
Route::get('siswa', 'SiswaController@daftar');
Route::post('/add-siswa', 'SiswaController@create');

//validasi
Route::get('validasi-siswa', function () {
    return view('pages.validasi-siswa');
});
Route::post('validasi', 'KartuGiziController@validasi');

//cek-gizi
Route::get('cek-gizi/{id}', 'KartuGiziController@index');
Route::post('hitung-gizi', 'KartuGiziController@create');
Route::get('hasil-gizi/{id}', 'KartuGiziController@hasil');

//input hasil di score
Route::post('add-score', 'KartuGiziController@addHasil');

//menampilkan data 
Route::get('data-siswa', 'SiswaController@index');
Route::get('edit/{id}','SiswaController@edit');
Route::get('delete/{id}','SiswaController@delete');
Route::post('update-siswa/{id}','SiswaController@update');

Route::get('data-gizi', 'DataGiziController@index');

//grafik
Route::get('grafik', 'GrafikController@index');


//sekolah
Route::get('sekolah', function () {
    return view('pages.input-sekolah');
});
Route::post('/add-sekolah', 'SekolahController@create');
Route::get('data-sekolah', 'SekolahController@index');

Route::get('export-data/{id}', 'GiziExportController@export');

Route::get('reset-data','ResetController@index');
Route::get('reset-data/save','ResetController@save');
//login
Route::get('login', function () {
    return view('login');
});
Route::post('auth', 'AuthController@proses');

?>


