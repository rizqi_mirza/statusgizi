<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableKartu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kartu', function (Blueprint $table) {
            $table->bigIncrements('id_kartu',11);
            $table->bigInteger('id_siswaKartu')->unsigned();
            $table->foreign('id_siswaKartu')->references('id_siswa')->on('siswas');
            $table->string('kelas',30);
            $table->string('berat_badan',30);
            $table->string('tinggi_badan',30);
            $table->date('tgl_ukur');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kartu');
    }
}
