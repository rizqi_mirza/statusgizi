<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableImtU extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('imt_u', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('tahun', 10);
            $table->string('bulan', 10);
            $table->string('sdmin3', 10);
            $table->string('sdmin2', 10);
            $table->string('sdmin1', 10);
            $table->string('mean', 10);
            $table->string('sdplus1', 10);
            $table->string('sdplus2', 10);
            $table->string('sdplus3', 10);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('imt_u');
    }
}
