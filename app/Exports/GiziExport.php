<?php

namespace App\Exports;

use App\Models\Score;
use App\Models\Siswa;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\FromCollection;

class GiziExport implements FromQuery, WithHeadings
{
    use Exportable;
    public function __construct(int $id){
        $this->id = $id;
    }

    public function query()
    {
        return 
            Score::select('nama_sekolah','nisn','nis','nama_siswa','tgl_lahir','jenis_kelamin','kelas','berat_badan','tinggi_badan'
                ,'tgl_ukur','z_core','keterangan')
                ->join('kartu','id_kartuScore','=','kartu.id_kartu')
                ->join('siswas','id_siswaKartu','=','siswas.id_siswa')
                ->join('sekolah','id_sekolahSiswa','=','sekolah.id_sekolah')
                ->where('sekolah.id_sekolah','=',$this->id);
    }
    
    // public function collection()
    // {
    //     return Siswa::all('nisn','nis','nama_siswa','tgl_lahir','jenis_kelamin')
    //     ->where('sekolah.id','=',$this->id);
    // }

    public function headings(): array
    {
        return [
            'Sekolah',
            'NISN',
            'NIS',
            'Nama',
            'Tanggal Lahir',
            'Jenis Kelamin',
            'Kelas',
            'Berat Badan',
            'Tinggi Badan',
            'Tanggal Pengukuran',
            'IMT/U',
            'Keterangan'
            
        ];
    }

}

?>