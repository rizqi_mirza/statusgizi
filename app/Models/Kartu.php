<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kartu extends Model
{
    protected $table  ="kartu";
    protected $primaryKey = 'id_kartu';
    protected $fillable = ['kelas','berat_badan','tinggi_badan','tgl_ukur'];
}
