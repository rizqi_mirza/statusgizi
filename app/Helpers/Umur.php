<?php

if(!function_exists('get_umur')){
   function get_umur($tglLahir, $tgl){
      $lahir = new DateTime($tglLahir);
      $tgl   = new DateTime($tgl);

      $diff  = $lahir->diff($tgl);
      $umur = array('tahun'=>$diff->y,'bulan'=>$diff->m);
      
      return $umur;
   }
}

?>