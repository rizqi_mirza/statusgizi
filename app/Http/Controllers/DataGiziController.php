<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
use App\Models\Kartu;
use App\Models\Score;




class DataGiziController extends Controller
{
    public function index(){
        $score = Score::join('kartu','score.id_kartuScore','=','kartu.id_kartu')->join('siswas', 'kartu.id_siswaKartu','=','siswas.id_siswa')
        ->join('sekolah','siswas.id_sekolahSiswa','=','sekolah.id_sekolah')->get();
        return view('pages.data-gizi', compact('score'));

    }

    public function delete(){
        $flight = Score::all();
        $flight->restore();
    }
}
