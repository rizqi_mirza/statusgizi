<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
use App\Models\Kartu;
use App\Models\Imt_laki;
use App\Models\Imt_u;
use App\Models\Score;
use DB;

class KartuGiziController extends Controller
{
    public function validasi(Request $request){
        $this->validate($request , [
            "nisn"          => "required"
        ]);

        $siswas = Siswa::where('nisn', $request->nisn)->get()->first();

        if (!empty($siswas)){
            return redirect("/cek-gizi/$siswas->id_siswa");
        }else{
            $request->session()->flash('status','NISN tidak terdaftar');
            return redirect('/validasi-siswa');
        }

    }

    public function index(Request $request,$id){
        $siswa = Siswa::select('siswas.id_siswa as id_sekolah','sekolah.*','siswas.*')->join('sekolah','siswas.id_sekolahSiswa','=','sekolah.id_sekolah')->where('siswas.id_siswa', $id)->get()->first();
        $curr_date = date("Y-m-d");
        $umur = get_umur($siswa->tgl_lahir,$curr_date);
        // dd($siswa);
        return view('pages.form-gizi',compact('siswa','umur','curr_date'));

    }

    public function create(Request $request){
        $this->validate($request , [
            "tgl_ukur"     => "required",
            "kelas"        => "required",
            "berat_badan"  => "required",
            "tinggi_badan" => "required"
        ]);

        $add = new Kartu;
        $add->tgl_ukur = $request->tgl_ukur;
        $add->berat_badan = $request->berat_badan;
        $add->tinggi_badan = $request->tinggi_badan;
        $add->kelas = $request->kelas;
        $add->id_siswaKartu = $request->id_siswa;  
        $add->save();
        
        return redirect('hasil-gizi/'.$add->id_kartu);
    }

    public function hasil(Request $request, $id){
        $kartu = Kartu::select('kartu.id_kartu as id_siswa','siswas.*','kartu.*')
        ->join('siswas','kartu.id_siswaKartu','=','siswas.id_siswa')
        ->join('sekolah','siswas.id_sekolahSiswa','=','sekolah.id_sekolah')->where('kartu.id_kartu', $id)->get()->first();
        // dd($kartu);
        $umur = get_umur($kartu->tgl_lahir,$kartu->tgl_ukur);

        $hasil_imt = $this->HitungIMT($kartu->berat_badan, $kartu->tinggi_badan);
        $hasil_z_core = $this->HitungZScore($umur,$kartu->jenis_kelamin,$hasil_imt['score']);

        return view('pages.hasil-gizi',compact('kartu','umur','hasil_imt','hasil_z_core'));
    }

    protected function HitungIMT($bb,$tb){
        $tb = $tb/100;
        $score = (float)$bb / pow((float)$tb,2);
        $keterangan = "";

        if($score<18.5)
            $keterangan = "Kurus/Kurang";
        elseif($score >= 18.5 && $score < 24.9 )
            $keterangan = "Normal";
        elseif($score >= 25 && $score < 27 )
            $keterangan = "Overweight";
        else
            $keterangan = "Obesitas";
        
        return compact('score','keterangan');
    }

    protected function HitungZScore($umur,$jk,$score){
        $jk = strtoupper($jk);

        if($jk == "LAKI-LAKI"){
            $imt = Imt_laki::where('tahun', $umur['tahun'])->where('bulan', $umur['bulan'])->get()->first();
        }else{
            $imt = Imt_u::where('tahun', $umur['tahun'])->where('bulan', $umur['bulan'])->get()->first();
        }

        if((float)$score<(float)$imt->mean){
            $zscore = ((float)$score-(float)$imt->mean)/((float)$imt->mean - (float)$imt->sdmin1);
        }elseif((float)$score>(float)$imt->mean){
            $zscore = ((float)$score-(float)$imt->mean)/((float)$imt->sdplus1 - (float)$imt->mean);
        }else{
            $zscore = ((float)$score-(float)$imt->mean)/(float)$imt->mean;
        }
        $keterangan = "";
        
        if($zscore < -2)
            $keterangan = "Gizi Kurang (kurus sekali)";
        elseif($zscore >= -2  && $zscore <= 1 )
            $keterangan = "Gizi Baik";
        elseif($zscore > 1  && $zscore <= 2 )
            $keterangan = "Gizi lebih";
        else
            $keterangan = "Obesitas";

        return compact('keterangan','zscore');
    }

   public function addHasil(Request $request){
    $this->validate($request , [
        "hasil_imt"     => "required",
        "z_core"        => "required",
        "keterangan"        => "required",
        
    ]);

    $add = new Score;
    $add->hasil_imt = $request->hasil_imt;
    $add->z_core = $request->z_core;
    $add->keterangan = $request->keterangan;
    $add->id_kartuScore = $request->id_kartu;  
    $add->save();

    return redirect('data-gizi/');
    
   }

   

}
