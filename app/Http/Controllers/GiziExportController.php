<?php

namespace App\Http\Controllers;

use App\Exports\GiziExport;
use Maatwebsite\Excel\Facades\Excel;

class GiziExportController extends Controller 
{
   public function export($id) 
   {
      ob_end_clean();
      ob_start();
      return (new GiziExport($id))->download('data_gizi_'.$id.'.xlsx');
   }
}

?>