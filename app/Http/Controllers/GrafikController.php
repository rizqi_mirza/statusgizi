<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Score;
use DB;
class GrafikController extends Controller
{
    public function index(){
        $dataScore = Score::select(DB::raw("count(score.id_score) as count"),"sekolah.*","score.*")->join('kartu','id_kartuScore','=','kartu.id_kartu')
        ->join('siswas','id_siswaKartu','=','siswas.id_siswa')
        ->join('sekolah','id_sekolahSiswa','=','sekolah.id_sekolah')->groupBy('sekolah.id_sekolah','score.keterangan')->orderBy('sekolah.id_sekolah', 'ASC')->get()
        ;
        // dd($dataScore);
        $label = array();
        $data  = array();
        $data['kurang'] = array();
        $data['baik'] = array();
        $data['lebih'] = array();
        $data['obesitas'] = array();

        foreach($dataScore as $value){
            if(!in_array($value->nama_sekolah,$label))
                $label[] = $value->nama_sekolah;

            if($value->keterangan == "Gizi Kurang (kurus sekali)")
                $data['kurang'][] = (int)$value->count;
            elseif($value->keterangan == "Gizi Baik")
                $data['baik'][] = (int)$value->count;
            elseif($value->keterangan == "Gizi lebih")
                $data['lebih'][] = (int)$value->count;
            else
                $data['obesitas'][] = (int)$value->count;
        }
        
        return view('pages.grafik',compact('label','data'));
    }
}
