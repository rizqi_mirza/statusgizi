<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ResetController extends Controller
{
   public function index(){    
      return view('pages.reset-data');
   }

   public function save(Request $request){
      
      DB::table('score')->delete();
      DB::table('kartu')->delete();
 
      $request->session()->flash('status','Data Score dan Kartu berhasil dihapus');
      return redirect("/reset-data");
  }
}