<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sekolah;

class SekolahController extends Controller
{
    public function index(){
        $sekolahs     = Sekolah::all();
        return view('pages.data-sekolah', compact('sekolahs'));
    }

    public function create(Request $request){
        $this->validate($request , [
            "nama_sekolah" => "required"]);

        $add = new Sekolah;
        $add->nama_sekolah = $request->nama_sekolah;
        $add->save();
        return redirect("/data-sekolah");
    }
}
