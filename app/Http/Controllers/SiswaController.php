<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\DB;
use App\Models\Siswa;
use App\Models\Sekolah;
// use DB;
// use Schema;

class SiswaController extends Controller
{
    public function index(){
        $siswas     = Siswa::join('sekolah','siswas.id_sekolahSiswa','=','sekolah.id_sekolah')->get();
        return view('pages.data-siswa', compact('siswas'));
    }

    public function daftar(){
        $items = Sekolah::all(['id_sekolah', 'nama_sekolah']);
        return view('pages.registrasi-siswa', compact('items'));
    }

    public function create(Request $request){
        $this->validate($request , [
            "id_sekolah" => "required",
            "nisn"         => "required",
            "nis"          => "required",
            "nama_siswa"   => "required",
            "tgl_lahir"    => "required",
            "jenis_kelamin"=> "required"
        ]);

        $add = new Siswa;
        $add->id_sekolahSiswa = $request->id_sekolah;
        $add->nisn = $request->nisn;
        $add->nis = $request->nis;
        $add->nama_siswa = $request->nama_siswa;
        $add->tgl_lahir = $request->tgl_lahir;
        $add->jenis_kelamin = $request->jenis_kelamin;  
        $result = $add->save();
        // dd($result); 
        if($result){
            $request->session()->flash('status','Data Siswa Berhasil Ditambahkan');
            return redirect("/data-siswa");
        }
        $request->session();
    }

    public function edit($id){
        $siswa = Siswa::where('id_siswa',$id)->first();
        $items = Sekolah::all(['id_sekolah', 'nama_sekolah']);
        return view('pages.edit-siswa', compact('siswa','items'));
    }

    public function update(Request $request,$id){
        $siswa = Siswa::find($id);
        $siswa->id_sekolahSiswa = $request->id_sekolah;
        $siswa->nisn = $request->nisn;
        $siswa->nis = $request->nis;
        $siswa->nama_siswa = $request->nama_siswa;
        $siswa->tgl_lahir = $request->tgl_lahir;
        $siswa->jenis_kelamin = $request->jenis_kelamin;
        $siswa->save();
        return redirect('/data-siswa');
    }


    public function delete($id){
        $siswa = Siswa::find($id);
        $siswa->delete();
        return redirect('/data-siswa');
    }

    
}
