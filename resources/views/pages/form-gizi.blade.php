@extends('master')

@section('main')
<div class="page-content">            
                <!-- Page Header -->
                <div class="page-header">
                    <div class="search-form">
                        <form action="#" method="GET">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control search-input" placeholder="Type something...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="close-search" type="button"><i class="icon-close"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <div class="logo-sm">
                                    <a href="javascript:void(0)" id="sidebar-toggle-button"><i class="fa fa-bars"></i></a>
                                    <a class="logo-box" href="index.html"><span>ecaps</span></a>
                                </div>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                        
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><a href="javascript:void(0)" id="collapsed-sidebar-toggle-button"><i class="fa fa-bars"></i></a></li>
                                    <li><a href="javascript:void(0)" id="toggle-fullscreen"><i class="fa fa-expand"></i></a></li>
                                    <!-- <li><a href="javascript:void(0)" id="search-button"><i class="fa fa-search"></i></a></li> -->
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="http://via.placeholder.com/36x36" alt="" class="img-circle"></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Profile</a></li>
                                            <li><a href="#">Calendar</a></li>
                                            <li><a href="#"><span class="badge pull-right badge-danger">42</span>Messages</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Account Settings</a></li>
                                            <li><a href="#">Log Out</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div><!-- /Page Header -->
                <!-- Page Inner -->
                <div class="page-inner">
                    <div class="page-title">
                        <h3 class="breadcrumb-header">Kartu Gizi</h3>
                    </div>
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div id="rootwizard">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Info Data Siswa</a></li>
                                        </ul>
                                        <div class="progress progress-sm m-t-sm">
                                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            </div>
                                        </div>
                                        <form id="wizardForm">
                                            <div class="tab-content">
                                                <div class="tab-pane active fade in" id="tab1">
                                                    <div class="row m-b-lg">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Nama Sekolah</label>
                                                                    <input type="text" class="form-control" name="exampleInputName" id="exampleInputName" value="{{$siswa->nama_sekolah}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputEmail">No. Induk Nasional</label>
                                                                    <input type="text" class="form-control" name="exampleInputEmail" id="exampleInputEmail" value="{{$siswa->nisn}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputEmail">No. Induk</label>
                                                                    <input type="text" class="form-control" name="exampleInputEmail" id="exampleInputEmail" value="{{$siswa->nis}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Nama Siswa</label>
                                                                    <input type="text" class="form-control" name="exampleInputName" id="exampleInputName" value="{{$siswa->nama_siswa}}"readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Tanggal Lahir</label>
                                                                    <input type="text" class="form-control" name="tgl_lahir" id="tgl_lahir" value="{{$siswa->tgl_lahir}}"readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Jenis Kelamin</label>
                                                                    <input type="text" class="form-control" name="jenis_kelamin" id="exampleInputName" value="{{$siswa->jenis_kelamin}}" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <!-- <ul class="pager wizard">
                                                    <li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
                                                    <li class="next"><a href="#" class="btn btn-default">Submit</a></li>
                                                </ul> -->
                                            </div>
                                        </form>
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Kartu Cek Gizi</a></li>
                                        </ul>
                                        <div class="progress progress-sm m-t-sm">
                                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            </div>
                                        </div>
                                        <form id="wizardForm" method="POST" action="{{url('/hitung-gizi')}}">
                                            {{ csrf_field() }}
                                            <div class="tab-content">
                                                <div class="tab-pane active fade in" id="tab1">
                                                    <div class="row m-b-lg">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="form-group col-md-6">
                                                                    <input type="hidden" class="form-control" name="id_siswa" id="exampleInputName" value="{{$siswa->id_siswa}}">
                                                                    <label for="exampleInputName">Tanggal Pengukuran</label>
                                                                    <input type="date" class="form-control" name="tgl_ukur" id="tgl_ukur" value="{{$curr_date}}" onchange="SetUmur(this)">
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Kelas</label>
                                                                    <input type="text" class="form-control" name="kelas" id="exampleInputName">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputEmail">Berat Badan</label>
                                                                    <input type="text" class="form-control" name="berat_badan" id="exampleInputEmail">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputEmail">Tinggi Badan</label>
                                                                    <input type="text" class="form-control" name="tinggi_badan" id="exampleInputEmail">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputEmail">Umur</label>
                                                                    <input type="text" class="form-control" name="exampleInputEmail" id="umur" value="{{$umur['tahun'].' Tahun '.$umur['bulan'].' Bulan'}}" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <ul class="pager wizard">
                                                    <!-- <li class="previous"><a href="#" class="btn btn-default">Previous</a></li> -->
                                                    <li class="next"><button class="btn btn-default">Submit</button></li>
                                                </ul>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
                
                
                
            </div>
            <script src="{{ ('assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
            <script src="{{ ('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
            <script>
                function SetUmur(el){
                    var tgl_ukur = new Date(el.value)
                    var tgl_lahir = new Date (document.getElementById('tgl_lahir').value);
                    var months;
                    var year;

                    months = tgl_ukur.getMonth() - tgl_lahir.getMonth() + (12 * (tgl_ukur.getFullYear() - tgl_lahir.getFullYear()));                   
                    if(tgl_ukur.getDay() > tgl_lahir.getDay()){
                        months = months-1
                    }
                    year = parseInt(months/12);
                    sisa_bulan = months - (year*12);
                    var umur = year+" Tahun "+sisa_bulan+" Bulan";
                    document.getElementById('umur').value = umur
                }   
            </script>
@stop