@extends('master')
@section('main')
<div class="page-content">            
                <!-- Page Header -->
                <div class="page-header">
                    <div class="search-form">
                        <form action="#" method="GET">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control search-input" placeholder="Type something...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="close-search" type="button"><i class="icon-close"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <div class="logo-sm">
                                    <a href="javascript:void(0)" id="sidebar-toggle-button"><i class="fa fa-bars"></i></a>
                                    <a class="logo-box" href="index.html"><span>ecaps</span></a>
                                </div>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                        
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><a href="javascript:void(0)" id="collapsed-sidebar-toggle-button"><i class="fa fa-bars"></i></a></li>
                                    <li><a href="javascript:void(0)" id="toggle-fullscreen"><i class="fa fa-expand"></i></a></li>
                                    <!-- <li><a href="javascript:void(0)" id="search-button"><i class="fa fa-search"></i></a></li> -->
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="http://via.placeholder.com/36x36" alt="" class="img-circle"></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Profile</a></li>
                                            <li><a href="#">Calendar</a></li>
                                            <li><a href="#"><span class="badge pull-right badge-danger">42</span>Messages</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Account Settings</a></li>
                                            <li><a href="#">Log Out</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div><!-- /Page Header -->
                <!-- Page Inner -->
                <div class="page-inner">
                    <div class="page-title">
                        <h3 class="breadcrumb-header">Grafik Status Gizi</h3>
                    </div>
                    
                    <div class="alert alert-default" role="alert">
                        <!-- Simple, clean and engaging charts for designers and developers. -->
                    </div>
                    
                    <div id="main-wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="panel panel-white">
                                    <div class="panel-body">
                                        <div class="panel-heading clearfix">
                                            <h4 class="panel-title">Bar</h4>
                                        </div>
                                        <canvas id="bar-chart"></canvas>
                                    </div>
                                </div>
                                
                            </div>
                        </div><!-- Row -->
                        
                        
                    </div><!-- Main Wrapper -->
                    
            </div>
<script>
$(document).ready(function() {
    var ctx = document.getElementById('bar-chart').getContext('2d');

    var barChartData = {
			labels:  @json($label),
			datasets: [{
				label: 'Gizi Kurang (kurus sekali)',
				backgroundColor: "#F44336",
				borderColor: "#F44336",
				borderWidth: 1,
				data:@json($data['kurang'])
			}, {
				label: 'Gizi Baik',
				backgroundColor: "#4CAF50",
				borderColor: "#4CAF50",
				borderWidth: 1,
				data: @json($data['baik'])
			}, {
				label: 'Gizi Lebih',
				backgroundColor: "#03A9F4",
				borderColor: "#03A9F4",
				borderWidth: 1,
				data: @json($data['lebih'])
			}, {
				label: 'Obesitas',
				backgroundColor: "#3F51B5",
				borderColor: "#3F51B5",
				borderWidth: 1,
				data: @json($data['obesitas'])
			}]

		};
    var barOptions = {
		responsive: true,
		legend: {
		    position: 'top',
		},
		title: {
			display: true,
			text: 'Gizi'
        },
        scales: {
            yAxes: [{
                ticks: {
                    stepSize: 1,
                    maxTicksLimit: 4,
                    suggestedMin: 0,
                }
            }]
        },
	};
    window.Chart = new Chart(ctx, {
        type: 'bar',
        data: barChartData,
        options: barOptions
	});
})
</script>
@stop