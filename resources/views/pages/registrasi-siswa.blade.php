@extends('master')

@section('main')
<div class="page-content">            
                <!-- Page Header -->
                <div class="page-header">
                    <div class="search-form">
                        <form action="#" method="GET">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control search-input" placeholder="Type something...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="close-search" type="button"><i class="icon-close"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <div class="logo-sm">
                                    <a href="javascript:void(0)" id="sidebar-toggle-button"><i class="fa fa-bars"></i></a>
                                    <a class="logo-box" href="index.html"><span>ecaps</span></a>
                                </div>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                        
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><a href="javascript:void(0)" id="collapsed-sidebar-toggle-button"><i class="fa fa-bars"></i></a></li>
                                    <li><a href="javascript:void(0)" id="toggle-fullscreen"><i class="fa fa-expand"></i></a></li>
                                    <!-- <li><a href="javascript:void(0)" id="search-button"><i class="fa fa-search"></i></a></li> -->
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="http://via.placeholder.com/36x36" alt="" class="img-circle"></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Profile</a></li>
                                            <li><a href="#">Calendar</a></li>
                                            <li><a href="#"><span class="badge pull-right badge-danger">42</span>Messages</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Account Settings</a></li>
                                            <li><a href="#">Log Out</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div><!-- /Page Header -->
                <!-- Page Inner -->
                <div class="page-inner">
                    <div class="page-title">
                        <!-- <h3 class="breadcrumb-header">Form Wizard</h3> -->
                    </div>
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div id="rootwizard">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Registrasi Siswa</a></li>
                                        </ul>
                                        <div class="progress progress-sm m-t-sm">
                                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            </div>
                                        </div>
                                        <form id="wizardForm" method="POST" action="{{url('/add-siswa')}}">
                                        {{ csrf_field() }}
                                            <div class="tab-content">
                                                <div class="tab-pane active fade in" id="tab1">
                                                    <div class="row m-b-lg">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <!-- <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Nama Sekolah</label>
                                                                    <input type="text" class="form-control" name="nama_sekolah" id="exampleInputName">
                                                                </div> -->
                                                                <div class="form-group">
                                                                    <label class="col-sm-12 control-label">Nama Sekolah</label>
                                                                    <div class="col-sm-12">
                                                                        <select style="margin-bottom:15px;" class="form-control" name="id_sekolah" >
                                                                        <option value="">-</option>
                                                                        @foreach($items as $item)
                                                                            <option value="{{$item->id_sekolah}}">{{$item->nama_sekolah}}</option>
                                                                        @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputEmail">No. Induk Nasional</label>
                                                                    <input type="text" class="form-control" name="nisn" id="exampleInputEmail">
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputEmail">No. Induk</label>
                                                                    <input type="text" class="form-control" name="nis" id="exampleInputEmail">
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Nama Siswa</label>
                                                                    <input type="text" class="form-control" name="nama_siswa" id="exampleInputName">
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Tanggal Lahir</label>
                                                                    <input type="text" class="form-control" name="tgl_lahir" id="exampleInputName">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="col-sm-12 control-label">Jenis Kelamin</label>
                                                                    <div class="col-sm-12">
                                                                        <select style="margin-bottom:15px;" class="form-control" name="jenis_kelamin" >
                                                                        <option value="">-</option>
                                                                        <option value="Laki-laki">Laki-Laki</option>
                                                                        <option value="Perempuan">Perempuan</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <h3>Regritasi Siswa</h3>
                                                            <p>Merupakan pencatatan data yang ada pada tiap sekolah. Adapun registrasi data meliputi :</p>
                                                            <p>- Sekolah </p>
                                                            <p>- No. Induk Nasional</p>
                                                            <p>- No. Induk</p>
                                                            <p>- Nama Siswa</p>
                                                            <p>- Tanggal Lahir</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="pager wizard">
                                                    <!-- <li class="previous"><a href="#" class="btn btn-default">Previous</a></li> -->
                                                    <li class="next"><button class="btn btn-default">Submit</button></li>
                                                </ul>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
                
                
                
            </div>
            <script src="{{ ('assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
            <script src="{{ ('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
@stop