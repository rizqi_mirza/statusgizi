@extends('master')

@section('main')
<div class="page-content">            
                <!-- Page Header -->
                <div class="page-header">
                    <div class="search-form">
                        <form action="#" method="GET">
                            <div class="input-group">
                                <input type="text" name="search" class="form-control search-input" placeholder="Type something...">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" id="close-search" type="button"><i class="icon-close"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <div class="logo-sm">
                                    <a href="javascript:void(0)" id="sidebar-toggle-button"><i class="fa fa-bars"></i></a>
                                    <a class="logo-box" href="index.html"><span>ecaps</span></a>
                                </div>
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        
                            <!-- Collect the nav links, forms, and other content for toggling -->
                        
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav">
                                    <li><a href="javascript:void(0)" id="collapsed-sidebar-toggle-button"><i class="fa fa-bars"></i></a></li>
                                    <li><a href="javascript:void(0)" id="toggle-fullscreen"><i class="fa fa-expand"></i></a></li>
                                    <li><a href="javascript:void(0)" id="search-button"><i class="fa fa-search"></i></a></li>
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="javascript:void(0)" class="right-sidebar-toggle" data-sidebar-id="main-right-sidebar"><i class="fa fa-envelope"></i></a></li>
                                    <li class="dropdown">
                                        <a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell"></i></a>
                                        <ul class="dropdown-menu dropdown-lg dropdown-content">
                                            <li class="drop-title">Notifications<a href="#" class="drop-title-link"><i class="fa fa-angle-right"></i></a></li>
                                            <li class="slimscroll dropdown-notifications">
                                                <ul class="list-unstyled dropdown-oc">
                                                    <li>
                                                        <a href="#"><span class="notification-badge bg-primary"><i class="fa fa-photo"></i></span>
                                                            <span class="notification-info">Finished uploading photos to gallery <b>"South Africa"</b>.
                                                                <small class="notification-date">20:00</small>
                                                            </span></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="notification-badge bg-primary"><i class="fa fa-at"></i></span>
                                                            <span class="notification-info"><b>John Doe</b> mentioned you in a post "Update v1.5".
                                                                <small class="notification-date">06:07</small>
                                                            </span></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="notification-badge bg-danger"><i class="fa fa-bolt"></i></span>
                                                            <span class="notification-info">4 new special offers from the apps you follow!
                                                                <small class="notification-date">Yesterday</small>
                                                            </span></a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><span class="notification-badge bg-success"><i class="fa fa-bullhorn"></i></span>
                                                            <span class="notification-info">There is a meeting with <b>Ethan</b> in 15 minutes!
                                                                <small class="notification-date">Yesterday</small>
                                                            </span></a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="http://via.placeholder.com/36x36" alt="" class="img-circle"></a>
                                        <ul class="dropdown-menu">
                                            <li><a href="#">Profile</a></li>
                                            <li><a href="#">Calendar</a></li>
                                            <li><a href="#"><span class="badge pull-right badge-danger">42</span>Messages</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Account Settings</a></li>
                                            <li><a href="#">Log Out</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                        </div><!-- /.container-fluid -->
                    </nav>
                </div><!-- /Page Header -->
                <!-- Page Inner -->
                <div class="page-inner">
                    <div class="page-title">
                        <h3 class="breadcrumb-header">Kartu Gizi</h3>
                    </div>
                <div id="main-wrapper">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-white">
                                <div class="panel-body">
                                    <div id="rootwizard">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Info Data Siswa</a></li>
                                        </ul>
                                        <div class="progress progress-sm m-t-sm">
                                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            </div>
                                        </div>
                                        <form id="wizardForm">
                                            <div class="tab-content">
                                                <div class="tab-pane active fade in" id="tab1">
                                                    <div class="row m-b-lg">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Nama Sekolah</label>
                                                                    <input type="text" class="form-control" name="exampleInputName" id="exampleInputName" value="{{$kartu->nama_sekolah}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputEmail">No. Induk Nasional</label>
                                                                    <input type="text" class="form-control" name="exampleInputEmail" id="exampleInputEmail" value="{{$kartu->nisn}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputEmail">No. Induk</label>
                                                                    <input type="text" class="form-control" name="exampleInputEmail" id="exampleInputEmail" value="{{$kartu->nis}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Nama Siswa</label>
                                                                    <input type="text" class="form-control" name="exampleInputName" id="exampleInputName" value="{{$kartu->nama_siswa}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Tanggal Lahir</label>
                                                                    <input type="text" class="form-control" name="exampleInputName" id="exampleInputName" value="{{$kartu->tgl_lahir}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Jenis Kelamin</label>
                                                                    <input type="text" class="form-control" name="exampleInputName" id="exampleInputName" value="{{$kartu->jenis_kelamin}}" readonly>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                                <!-- <ul class="pager wizard">
                                                    <li class="previous"><a href="#" class="btn btn-default">Previous</a></li>
                                                    <li class="next"><a href="#" class="btn btn-default">Submit</a></li>
                                                </ul> -->
                                            </div>
                                        </form>
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#tab1" data-toggle="tab"><i class="fa fa-user m-r-xs"></i>Hasil Cek Gizi</a></li>
                                        </ul>
                                        <div class="progress progress-sm m-t-sm">
                                            <div class="progress-bar progress-bar-info progress-bar-striped active" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 0%">
                                            </div>
                                        </div>
                                        <form id="wizardForm" method="POST" action="{{url('/add-score')}}">
                                        {{ csrf_field() }}
                                            <div class="tab-content">
                                                <div class="tab-pane active fade in" id="tab1">
                                                    <div class="row m-b-lg">
                                                        <div class="col-md-6">
                                                            <div class="row">
                                                                
                                                                <div class="form-group col-md-6">
                                                                    <label for="exampleInputName">Tanggal Pengukuran</label>
                                                                    <input type="text" class="form-control" name="tgl_ukur" id="exampleInputName"  value="{{$kartu->tgl_ukur}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputName">Kelas</label>
                                                                    <input type="text" class="form-control" name="kelas" id="exampleInputName" value="{{$kartu->kelas}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputEmail">Berat Badan</label>
                                                                    <input type="text" class="form-control" name="berat_badan" id="exampleInputEmail" value="{{$kartu->berat_badan}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-4">
                                                                    <label for="exampleInputEmail">Tinggi Badan</label>
                                                                    <input type="text" class="form-control" name="tinggi_badan" id="exampleInputEmail" value="{{$kartu->tinggi_badan}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputEmail">Umur</label>
                                                                    <input type="text" class="form-control" name="umur" id="exampleInputEmail" value="{{$umur['tahun'].' Tahun '.$umur['bulan'].' Bulan '}}" readonly>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <h3>Hasil Gizi Siswa</h3>
                                                                    <input type="hidden" class="form-control" name="id_kartu" id="exampleInputName" value="{{$kartu->id_kartu}}">
                                                            <p>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputEmail">Berdasarkan IMT</label>
                                                                    <input type="text" class="form-control" name="hasil_imt" id="exampleInputEmail" value="{{round($hasil_imt['score'],2)}}" readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <input type="text" class="form-control" name="hasil_imt" id="exampleInputEmail" value="{{$hasil_imt['keterangan']}}" readonly>
                                                                    <!-- <strong>{{$hasil_imt['keterangan']}}</strong> -->
                                                                </div>
                                                            </p>
                                                           
                                                            <p>
                                                                <div class="form-group col-md-12">
                                                                    <label for="exampleInputEmail">Berdasarkan IMT/U</label>
                                                                    <input type="text" class="form-control" name="z_core" id="exampleInputEmail" value="{{round($hasil_z_core['zscore'],2)}} " readonly>
                                                                </div>
                                                                <div class="form-group col-md-12">
                                                                    <input type="text" class="form-control" name="keterangan" id="exampleInputEmail" value="{{$hasil_z_core['keterangan']}} " readonly>
                                                                </div>
                                                                <!-- <strong>{{$hasil_z_core['keterangan']}}</strong> -->
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <ul class="pager wizard">
                                                    <!-- <li class="previous"><a href="#" class="btn btn-default">Previous</a></li> -->
                                                    <li class="next"><button class="btn btn-default">Selesai</button></li>
                                                </ul>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->
                
                
                
            </div>
            <script src="{{ ('assets/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
            <script src="{{ ('assets/plugins/bootstrap-wizard/jquery.bootstrap.wizard.min.js')}}"></script>
        
@stop