<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Responsive Admin Dashboard Template">
        <meta name="keywords" content="admin,dashboard">
        <meta name="author" content="skcats">
        <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        <!-- Title -->
        <title>SIM Gizi - Sistem Informasi Manajemen Gizi</title>

        <!-- Styles -->
        <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
        <link href="{{url('/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{url('/assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{url('/assets/plugins/icomoon/style.css')}}" rel="stylesheet">
        <link href="{{url('/assets/plugins/uniform/css/default.css')}}" rel="stylesheet"/>
        <link href="{{url('/assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet"/>
        <link href="{{url('/assets/plugins/datatables/css/jquery.datatables.min.css')}}" rel="stylesheet" type="text/css"/>	
        <link href="{{url('/assets/plugins/datatables/css/jquery.datatables_themeroller.css')}}" rel="stylesheet" type="text/css"/>	
        <link href="{{url('/assets/plugins/bootstrap-datepicker/css/datepicker3.css')}}" rel="stylesheet" type="text/css"/>
      
        <!-- Theme Styles -->
        <link href="{{url('/assets/css/ecaps.min.css')}}" rel="stylesheet">
        <link href="{{url('/assets/css/custom.css')}}" rel="stylesheet">
        <script src="{{url('/assets/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!-- Page Container -->
        <div class="page-container">
            <!-- Page Sidebar -->
            <div class="page-sidebar">
                <a class="logo-box" href="{{url('/')}}">
                    <span>SIM Gizi</span>
                    <i class="icon-radio_button_unchecked" id="fixed-sidebar-toggle-button"></i>
                    <i class="icon-close" id="sidebar-toggle-button-close"></i>
                </a>
                <div class="page-sidebar-inner">
                    <div class="page-sidebar-menu">
                        <ul class="accordion-menu">
                        <li class="active-page">
                                <a href="{{url('/')}}">
                                    <i class="menu-icon icon-home4"></i><span>Dashboard</span>
                                </a>
                            </li>
                            
                            <li>
                                <a href="javascript:void(0);">
                                    <i class="menu-icon icon-code"></i><span>Pendaftaran</span><i class="accordion-icon fa fa-angle-left"></i>
                                </a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('/siswa')}}">Pendaftaran Siswa</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{url('/validasi-siswa')}}">
                                    <i class="menu-icon icon-inbox"></i><span>Kartu Gizi</span>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon icon-format_list_bulleted"></i><span>Data</span><i class="accordion-icon fa fa-angle-left"></i>
                                </a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('/data-siswa')}}">Data Siswa</a></li>
                                    <li><a href="{{url('/data-gizi')}}">Data Gizi </a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)">
                                    <i class="menu-icon icon-format_list_bulleted"></i><span>Sekolah</span><i class="accordion-icon fa fa-angle-left"></i>
                                </a>
                                <ul class="sub-menu">
                                    <li><a href="{{url('/sekolah')}}">Input Sekolah</a></li>
                                    <li><a href="{{url('/data-sekolah')}}">Daftar Sekolah </a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{url('/grafik')}}">
                                    <i class="menu-icon icon-show_chart"></i><span>Grafik</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{url('/reset-data')}}">
                                    <i class="fa fa-eraser"></i><span>Reset Data</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div><!-- /Page Sidebar -->
            
            <!-- Page Content -->
            @yield('main')
            <!-- end Page Content -->
        </div><!-- /Page Container -->
        
        
        <!-- Javascripts -->
        <script src="{{url('/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{url('/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{url('/assets/plugins/uniform/js/jquery.uniform.standalone.js')}}"></script>
        <script src="{{url('/assets/plugins/switchery/switchery.min.js')}}"></script>
        <script src="{{url('/assets/plugins/datatables/js/jquery.datatables.min.js')}}"></script>
        <script src="{{url('/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js')}}"></script>
        <script src="{{url('/assets/js/ecaps.min.js')}}"></script>
        <script src="{{url('/assets/js/pages/table-data.js')}}"></script>
        
        <script src="{{url('/assets/plugins/chartjs/chart.min.js') }}"></script>
        <script src="{{url('/assets/plugins/d3/d3.min.js') }}"></script>
        <script src="{{url('/assets/plugins/nvd3/nv.d3.min.js') }}"></script>
        <script src="{{url('/assets/plugins/flot/jquery.flot.min.js') }}"></script>
        <script src="{{url('/assets/plugins/flot/jquery.flot.time.min.js') }}"></script>
        <script src="{{url('/assets/plugins/flot/jquery.flot.symbol.min.js') }}"></script>
        <script src="{{url('/assets/plugins/flot/jquery.flot.resize.min.js') }}"></script>
        <script src="{{url('/assets/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
        <script src="{{url('/assets/plugins/flot/jquery.flot.pie.min.js') }}"></script>
        <script src="{{url('/assets/js/pages/chart.js') }}"></script>
    </body>
</html>