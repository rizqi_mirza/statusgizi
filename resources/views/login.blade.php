<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Responsive Admin Dashboard Template">
        <meta name="keywords" content="admin,dashboard">
        <meta name="author" content="skcats">
        <!-- The above 6 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        
        <!-- Title -->
        <title>ecaps - Responsive Admin Dashboard Template</title>

        <!-- Styles -->
        <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
        <link href="{{url('/assets/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{url('/assets/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{url('/assets/plugins/icomoon/style.css')}}" rel="stylesheet">
        <link href="{{url('/assets/plugins/uniform/css/default.css')}}" rel="stylesheet"/>
        <link href="{{url('/assets/plugins/switchery/switchery.min.css')}}" rel="stylesheet"/>
      
        <!-- Theme Styles -->
        <link href="{{url('/assets/css/ecaps.min.css')}}" rel="stylesheet">
        <link href="{{url('/assets/css/custom.css')}}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <!-- Page Container -->
        <div class="page-container">
                <!-- Page Inner -->
                <div class="page-inner login-page">
                    <div id="main-wrapper" class="container-fluid">
                        <div class="row">
                            <div class="col-sm-6 col-md-3 login-box">
                                <h4 class="login-title">Sign in </h4>
                                <form method="POST" action="{{url('/auth')}}">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">email</label>
                                        <input type="email" class="form-control" id="exampleInputEmail1" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Password</label>
                                        <input type="password" class="form-control" id="exampleInputPassword1" name="password">
                                    </div>
                                    <button class="btn btn-primary">Login</button>
                                </form>
                            </div>
                        </div>
                    </div>
            </div><!-- /Page Content -->
        </div><!-- /Page Container -->
        
        
        <!-- Javascripts -->
        <script src="{{url('/assets/plugins/jquery/jquery-3.1.0.min.js')}}"></script>
        <script src="{{url('/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>
        <script src="{{url('/assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
        <script src="{{url('/assets/plugins/uniform/js/jquery.uniform.standalone.js')}}"></script>
        <script src="{{url('/assets/plugins/switchery/switchery.min.js')}}"></script>
        <script src="{{url('/assets/js/ecaps.min.js')}}"></script>
    </body>
</html>